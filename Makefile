# Depends: rubber
DIR:=$(strip $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST)))))
ORGS = $(wildcard *.org)
TEXS = $(patsubst %.org,%.tex,$(ORGS))
PDFS = $(patsubst %.org,%.pdf,$(ORGS))
PDFHOS = $(patsubst %.org,%.handout.pdf,$(ORGS))

all: $(PDFS)

handouts: $(PDFHOS)

.PRECIOUS: $(TEXS)
%.tex: %.org
	$(DIR)/bin/export-org-to-latex $<

%.pdf: %.tex
	rubber -m pdftex $<

clean: $(patsubst %,%/clean,$(TEXS))

%/clean:
	if [ -f $* ]; then rubber -m pdftex --clean $* ; fi
	rm -f $*

distclean: clean

arxiv: swh-archive-reference-howto.pdf
	mkdir -p arxiv
	cp `grep images swh-archive-reference-howto.log | grep Info |sed 's/.*: //' | sed 's/ .*//'` arxiv/
	echo "%% -*- latex-command: pdflatex -*-" > arxiv/swh-archive-reference-howto.tex
	texexpand -b swh-archive-reference-howto.tex | sed 's%images/%%' >> arxiv/swh-archive-reference-howto.tex
