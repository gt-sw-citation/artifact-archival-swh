% Created 2020-05-27 Wed 17:55
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
%% -*- latex-command: pdflatex -*-
\usepackage{listings}
\usepackage{tcolorbox}
\usepackage{subcaption}
\usepackage{libertine}
\usepackage{lastpage}
\usepackage{a4wide}
\usepackage{alltt}
\usepackage[datamodel=software,doi=false,isbn=false,url=false,backend=biber,abbreviate=false,style=numeric]{biblatex}
\usepackage{software-biblatex}
\usepackage{syntax}
\addbibresource{biblio.bib}
\addbibresource{swh.bib}
\renewcommand{\UrlFont}{\ttfamily\small} \PassOptionsToPackage{hyphens}{url}\usepackage{hyperref}
\newcommand{\swhurl}[1]{https://archive.softwareheritage.org/#1}
\newcommand{\swhref}[2]{\href{\swhurl{#1}}{#2}}
\newcommand{\swhidref}[1]{\swhref{#1}{\small #1}}
\newcommand{\swhidex}[1]{\begin{tcolorbox}\swhidref{#1}\end{tcolorbox}\noindent}
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\usepackage{xstring}
\usepackage{catchfile}
\CatchFileDef{\headfull}{.git/HEAD}{}
\StrGobbleRight{\headfull}{1}[\head]
\StrBehind[2]{\head}{/}[\branch]
\CatchFileDef{\commit}{.git/refs/heads/\branch}{}
\newcommand{\therevision}{\thanks{This revision: \texttt{\commit} on branch \texttt{\branch}}}
\lstset{columns=fixed,basicstyle=\ttfamily}
\author{Roberto Di Cosmo\\Inria, Software Heritage, University of Paris, France\\roberto@dicosmo.org}
\date{May 2020}
\title{How to use Software Heritage for archiving and referencing your source code: guidelines and walkthrough\therevision}
\hypersetup{
 pdfauthor={dicosmo},
 pdftitle={How to use Software Heritage for archiving and referencing your source code: guidelines and walkthrough\therevision},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.1 (Org mode 9.1.14)}, 
 pdflang={English}}
\begin{document}

\maketitle
Software source code is \emph{an essential research output}, and there is a growing
general awareness of its importance for supporting the research process
\cite{Borgman2012,Stodden-reprod-2012,Hinsen2013}.  Many research communities
strongly encourage making the source code of the artefact available by archiving
it in publicly-accessible long-term archives. Some have even put in place
mechanisms to assess research software, like the \emph{Artefact Evaluation} process introduced
in 2011 and now widely adopted by many computer science conferences \cite{Dagstuhl-Artefacts-2016},
and the \emph{Artifact Review and Badging} program of the ACM \cite{AcmBadges}.\\

Software Heritage \cite{swhipres2017,swhcacm2018} is a non profit, long term
universal archive specifically designed for software source code, and able to
store not only a software artifact, but \emph{also its full development history}. 
It provides the ideal place to \emph{preserve research software
artifacts}, and offers powerful mechanisms to \emph{enhance research articles} with
precise references to relevant fragments of your source code.\\

Using Software Heritage for your research software artifacts is straightforward
and involves three simple steps, described in the picture below:

\begin{center}
\begin{center}
\includegraphics[width=.8\textwidth]{images/swh3steps.pdf}
\end{center}
\end{center}

In this document we will go through each of these three steps, providing
guidelines for making the most out of Software Heritage for your research:
Section \ref{sec:prepare} describes the best practices for preparing your source
code for archival; Section \ref{sec:save} shows how to archive your code in
Software Heritage; Section \ref{sec:walkthrough} shows the rich functionalities
you can use for referencing in your article source code archived in Software
Heritage; finally, in the Appendix you will find a formal description of the
different kinds of identifiers available for adressing the content archived in
Software Heritage.
\section{Prepare your repository}
\label{sec:org7ba93ad}
\label{sec:prepare}

We assume that your source code is hosted on a repository publicly accessible
(Github, Bitbucket, a GitLab instance, an institutional software forge, etc.)
using one of the version control systems supported by Software Heritage,
currently Subversion, Mercurial and Git
\footnote{For up to date information, see \url{https://archive.softwareheritage.org/browse/origin/save/}}.\\

It is highly recommended that you provide, in your source code repository,
appropriate information on your research artifact: it will make it more
appealing and useful to future users (which might actually be \emph{you} in a few
months).

Well established best practice is to include, at the toplevel of your
source code tree, three \emph{key files}, README, AUTHORS and LICENSE, with
the information described below.

\begin{description}
\item[{README}] : A description of the software.\\
This file should contain \emph{at least}
\begin{itemize}
\item the name of the software/project
\item a brief description of the project.
\end{itemize}
It is also \emph{highly recommended} to add the following information
\begin{itemize}
\item pointers to the project website and documentation,
\item pointer to the project development platform,
\item license for the project (if not in a separate LICENSE file),
\item contact and support information,
\item build/installation instructions or a pointer to a file containing them (usually INSTALL)
\end{itemize}
In could be useful to provide here also some information for the users, like a list of features or informations on how to use the source code
\end{description}

\begin{description}
\item[{AUTHORS}] : The list of all authors that need to be credited for the current version.\\
If you want to specify the role of each contributor in this list, we suggest
to use the taxonomy of contributors presented in \cite{2020GtCitation}, which distinguishes
the following roles: \emph{Design, Architecture, Coding, Testing, Debugging, Documentation, Maintenance, Support, Management}.
\item[{LICENSE}] : The project license terms.\\
For Open Source Licenses, it is strongly recommended to use the standard names that can be found on the \url{https://spdx.org/licenses/} website.
\end{description}

Future users that find the artifact useful might want to give you credit by citing
it.  To this end, you should provide instructions on how you prefer the
artifact to be cited. 

Sophisticated support for bibliographic entries in BibTeX format is available
for users of the BibLaTeX package is provided by the \texttt{biblatex-software}
package \cite{biblatex-software}, available on CTAN \cite{ctan}.
Another option is to use the Citation File Format,
CFF (usually in a file named \textbf{citation.cff}).

We recommend to also provide structured metadata information in a machine
readable format. While practices in this area are still evolving, one can use
the CodeMeta generator available at \url{https://codemeta.github.io/codemeta-generator/}
to produce metadata conformant to the CodeMeta schema, and put the JSON-LD output
in a \textbf{codemeta.json} file at the root of the project.

\subsection{Learning more}
\label{sec:org2b642e6}
The seminal article \emph{Software Release Practice HOWTO} by E. S. Raymond
\cite{raymond2013} documents best practices and conventions for releasing
software that have been well established for decades, and form the basis
of most current recommendations. Interesting more recent resources include
the REUSE website \cite{reuse}, which provides detailed guidance
and tools to verify compliance with the guidelines, as well as \cite{SSI2018}, which focuses more on research software.

\section{Save your code}
\label{sec:org8d96825}
\label{sec:save}

\noindent Once your code repository has been properly prepared, you only need to:

\begin{itemize}
\item go to \url{https://archive.softwareheritage.org/browse/origin/save/},
\item pick your version control system in the drop-down list, enter the code repository url
\footnote{Make sure to use the clone/checkout url as given by the development platform hosting your code. It can easily be found in the web interface of the development platform.},
\item click on the Submit button (see Figure \ref{fig:savecodenow}).
\end{itemize}

\begin{figure}[h!]
\begin{center}
\includegraphics[width=\textwidth]{images/SaveCodeNow}
\end{center}
\caption{The Save Code Now form}\label{fig:savecodenow}
\end{figure}

\noindent \textbf{That's it, it's all done!} No need to create an account or to provide
personal information of any kind. If the url you provided is correct, Software
Heritage will archive your repository, with its full development history,
shortly after. If your repository is hosted on one of the major forges we
already know, this process will take just a few hours; if you point to a
location we never saw before, it can take longer, as we will need to manually
approve it.\\

\noindent \textbf{For hackers:} you can also request archival programmatically, using the
Software Heritage API
\footnote{For details, see \url{https://archive.softwareheritage.org/api/1/origin/save/}};
this can be quite handy to integrate, for example, into a Makefile.

\section{Reference your work}
\label{sec:orge2f7a37}
\label{sec:walkthrough}

Once the source code has been archived, the Software Heritage \emph{intrinsic
identifiers}, called SWH-ID, \href{https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html}{fully documented online} and shown in Figure
\ref{fig:orgcdad756}, can be used to reference with great ease any version of
it.

\begin{figure}[htbp]
\centering
\includegraphics[width=.6\linewidth]{images/swh-id.png}
\caption{\label{fig:orgcdad756}
Schema of the core Software Heritage identifiers}
\end{figure}

SWH-IDs are URIs with a very simple schema: the \texttt{swh} prefix makes
 explicit that these identifiers are related to Software Heritage; the colon
 (\verb|:|) is used as separator between the logical parts of identifiers; the
 schema version (currently \verb|1|) is the current version of this identifier
 schema; then follows the type of the objects identified and finally comes a
 hex-encoded (using lowercase ASCII characters) cryptographic signature of this
 object, computed in a standard way, as detailed in \cite{swhipres2018,cise-2020-doi}.
 These core identifiers may be equipped with the following \emph{qualifiers} that carry
 contextual \emph{extrinsic} information about the object:

\begin{description}
\item[{origin :}] the \emph{software origin} where an object has been found or observed in the wild, as an URI;
\item[{visit :}] persistent identifier of a \emph{snapshot} corresponding to a specific \emph{visit} of a repository containing the designated object;
\item[{anchor :}] a \emph{designated node} in the Merkle DAG relative to which a \emph{path to the object} is specified;
\item[{path :}] the \emph{absolute file path}, from the \emph{root directory} associated to the \emph{anchor node}, to the object;
\item[{lines :}] \emph{line number(s)} of interest, usually within a content object
\end{description}

The combination of the core SWH-IDs with these qualifiers provides a very
powerful means of referring in a research article to all the software artefacts
of interest.\\

We present here three common use cases: link to the \emph{full repository} archived in Software Heritage; link to a \emph{precise version of the software project},
and link to a \emph{precise version of a source code file}, down to the level of the line of code.

To make this concrete, in what follows we use as a running example the article
\emph{A ``minimal disruption'' skeleton experiment: seamless map and reduce embedding
in OCaml} by Marco Danelutto and Roberto Di Cosmo \cite{Parmap2012} published
in 2012. This article introduced a nifty library for multicore parallel
programming that was distributed via the \url{https://gitorious.org} collaborative
development platform, at \url{https://gitorious.org/parmap}. Since Gitorious has been
shut down a few years ago, like Google Code and CodePlex, this example is
particularly fit to show why pointing to an \emph{archive} that has your code is
better than pointing to the collaborative development platform where you
developed it.

\subsection{Full repository}
\label{sec:org553f320}
In Software Heritage, we keep track of all the \emph{origins} from which source
code has been retrieved, and finding a given \texttt{origin} is as easy as
adding in front of it the prefix
\url{https://archive.softwareheritage.org/browse/origin}

These origins are the exact \emph{URLs of the version control system} that a
developer would use to clone a working repository, and are the same urls that
you pass to the \emph{Save Code Now} form described in Section \ref{sec:save}.

In our running example, for the Parmap code on \emph{gitorious.org}, this origin
is \url{https://gitorious.org/parmap/parmap.git}, so the URL of the \emph{persistently
archived full repository} is:

\url{https://archive.softwareheritage.org/browse/origin/https://gitorious.org/parmap/parmap.git}

Just add this link to your article, and your readers will be able to get hold of
the archived copy of your repository even if/when the original development
platform goes away (as it has actually happened for \texttt{gitorious.org} that
has been shut down in 2015).

Your readers can then browse the contents of your repository extensively,
delving into its development history, and/or directory structure, down to each
single source code file
\footnote{For a guided tour see \url{https://www.softwareheritage.org/2018/09/22/browsing-the-software-heritage-archive-a-guided-tour/}}.

\textbf{N.B.}: if you are unsure about what is the actual origin URL of your
repository, you can look it up using the search box that is available at
\url{https://archive.softwareheritage.org/browse/search/}

\subsection{Specific version}
\label{sec:org474a365}

Pointing to the full archived repository is nice, but a version controlled
repository usually contains all the history of development of the source code,
whiche records different states of the project, usually called \emph{revisions}.

In order to support reproducibility of scientific results, we need to be able to
pinpoint precisely the state(s) of the source code used in the article.
Software Heritage provides a very easy means of pointing to a precise
\emph{revision}, via a standard identifier schema, called SWHID, which is
\href{https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html}{fully documented online} and is discussed in the article \cite{swhipres2018}.

In our running example, the Parmap article, the exact revision of the source
code of the library used therein has the following SWHID:

\begin{tcolorbox}
swh:1:rev:0064fbd0ad69de205ea6ec6999f3d3895e9442c2;\\
 origin=https://gitorious.org/parmap/parmap.git;\\
 visit=swh:1:snp:78209702559384ee1b5586df13eca84a5123aa82
\end{tcolorbox}

And you can turn this identifier into a clickable URL by prepending to it the
prefix \url{https://archive.softwareheritage.org/} (you can try it live right now by clicking on \href{https://archive.softwareheritage.org/swh:1:rev:0064fbd0ad69de205ea6ec6999f3d3895e9442c2;origin=https://gitorious.org/parmap/parmap.git;visit=swh:1:snp:78209702559384ee1b5586df13eca84a5123aa82}{this link}).

\subsection{Code fragment}
\label{sec:orgf40fd76}
A particularly nifty feature of the SWHIDs supported by Software Heritage is the
ability to pinpoint a fragment of code inside a specific version of a file, by
using the \texttt{lines=} qualifier available for identifiers that point to
files.

\begin{figure}[t]
\centering
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics[scale=0.5]{images/article-parmap-code}
        \caption{as presented in the article \cite{Parmap2012}}\label{fig:parmappaper}
    \end{subfigure}
    \vspace{1em}
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics[scale=0.45]{images/parmap-cnt}
        \caption{as archived in Software Heritage}\label{fig:parmapswh}
    \end{subfigure}
    \caption{Code fragment from the published article compared to the content
      in the Software Heritage archive}
  \label{fig:parmap}
\end{figure}

Let's see this feature at work in our running example, which shows clearly
how an article can be greatly enhanced by providing pointers to
code fragments.

In Figure 1 of \cite{Parmap2012}, which is shown here as Figure
\ref{fig:parmappaper}, the authors want to present the core part of the code
implementing the parallel functionality that constitutes the main contribution
of their article. The usual approach is to typeset in the article itself \emph{an
excerpt of the source code}, and let the reader try to find it by delving
into the code repository, which may have evolved in the mean time.
Finding the exact matching code can be quite difficult, as the code excerpt
is \emph{often edited} a bit with respect to the original, sometimes to drop
details that are not relevant for the discussion, and sometimes due to
space limitations.

In our case, the article presented 29 lines of code, slightly edited from the
43 actual lines of code in the Parmap library: looking at
\ref{fig:parmappaper}, one can easily see that some lines have been dropped
(102-103, 118-121), one line has been split (117) and several lines simplified
(127, 132-133, 137-142).\\

Using Software Heritage, the authors can do a much better job, because the original
code fragment can now be precisely identified by the following Software Heritage identifier,
that can be easily obtained using the permalink box shown in Section \ref{ssec:getswhid} above, and that
will \textbf{always} point to the code fragment shown in Figure \ref{fig:parmapswh}.\\
\begin{tcolorbox}
swh:1:cnt:d5214ff9562a1fe78db51944506ba48c20de3379;\\
 origin=https://gitorious.org/parmap/parmap.git;\\
 visit=swh:1:snp:78209702559384ee1b5586df13eca84a5123aa82;\\
 anchor=swh:1:rev:0064fbd0ad69de205ea6ec6999f3d3895e9442c2;\\
 path=/parmap.ml;\\
 lines=101-143
\end{tcolorbox}

The caption of the original article shown in Figure \ref{fig:parmappaper} can
then be significantly enhanced by incorporating all the clickable links needed
to point to the exact source code fragment that has been edited for inclusion
in the article, as shown in Figure \ref{fig:swhall} (notice that the percent signs
at the end of the line are necessary to ensure \LaTeX{} does not break the SWHIDs).

\begin{figure}[h!]
\begin{tcolorbox}
Simple implementation of the distribution, fork, and recollection phases in \texttt{Parmap} 
(slightly simplified from 
 \swhref{swh:1:cnt:d5214ff9562a1fe78db51944506ba48c20de3379;%
	 origin=https://gitorious.org/parmap/parmap.git;%
	 visit=swh:1:snp:78209702559384ee1b5586df13eca84a5123aa82;%
	 anchor=swh:1:rev:0064fbd0ad69de205ea6ec6999f3d3895e9442c2;%
	 path=/parmap.ml;%
	 lines=101-143
}{the actual code in the version of Parmap used for this article})
\end{tcolorbox}
\caption{A caption text with links to code fragment and revision}\label{fig:swhall}
\end{figure}

When clicking on the hyperlinked text in the caption shown above, the reader is
brought seamlessly to the Software Heritage archive on a page showing the
corresponding source code archived in Software Heritage, with the relevant
lines highlighted (see Figure \ref{fig:parmapswh}).

 \begin{figure}[h!]
 \begin{tcolorbox} 
 \scriptsize
 \begin{lstlisting} 
\newcommand{\swhurl}[1]{https://archive.softwareheritage.org/#1}
\newcommand{\swhref}[2]{\href{\swhurl{#1}}{#2}}

...

\caption{Simple implementation of the distribution,
fork, and recollection phases in \texttt{Parmap}
(slightly simplified from 
\swhref{swh:1:cnt:d5214ff9562a1fe78db51944506ba48c20de3379;%
	  origin=https://gitorious.org/parmap/parmap.git;%
	  visit=swh:1:snp:78209702559384ee1b5586df13eca84a5123aa82;%
	  anchor=swh:1:rev:0064fbd0ad69de205ea6ec6999f3d3895e9442c2;%
	  path=/parmap.ml;%
	  lines=101-143
 }{the actual code in the version of Parmap used for this article})
}
 \end{lstlisting}
 \end{tcolorbox}
 \caption{Adding clickable hyperlinks to Software Heritage in \LaTeX}\label{fig:swhref}
 \end{figure}

\LaTeX{} users can produce the caption of \ref{fig:swhall} using a few
convenient auxiliary macros, as shown in Figure \ref{fig:swhref}. They can also
create corresponding bibliography entries using the \texttt{biblatex-software} package, like
\cite{parmap-0.9.8} and \cite{simplemapper} found in the bibliography of this guide.

\subsubsection{Getting your SWHID}
\label{sec:orgb40d62d}
\label{ssec:getswhid}
 A very simple way of getting the right SWHID is to browse your archived code in
 Software Heritage, and to navigate to the revision you are interested in. Click
 then on the \emph{permalinks vertical red tab} that is present on all pages of the
 archive, and in the tab that opens up you select the \emph{revision} identifier:
 an example is shown in Figure \ref{fig:permalink}.

\begin{figure}[h]
  \centering
  \includegraphics[width=\linewidth]{images/permalink-box}
  \caption{Obtaining a Software Heritage identifier using the permalink box on
    the archive Web user interface}
  \label{fig:permalink}
\end{figure}

The two convenient buttons on the botton right allow you to copy the identifiers or the full permalink
in the clipboard, to insert in your article as you see fit.
Make sure to check the box at the bottom left to generate a SWHID with all relevant qualifiers
corresponding to your browsing context.

\subsubsection{Generating and verifying SWHIDs (for the geeks)}
\label{sec:org8253611}
Version 1 of the SWHIDs uses git-compatible hashes, so if you are using git as
a version control system, you can create the right SWHID by just prepending
\texttt{swh:1:rev:} to your commit hash. This might come pretty handy if you
plan to automate the generation of the identifiers to be included in your
article: you will always have your code and your article in sync!

Software Heritage identifiers can also be generated and verified independently
by anyone using \texttt{swh-identify}, an open source tool developed
by Software Heritage, and distributed via PyPI as \texttt{swh.model} (stable
version at 
\swhidref{swh:1:rev:6cab1cc81118877e2105c32b08653509475f3eaa;\\
origin=https://pypi.org/project/swh.model/}).

\section{Acknowledgements}
\label{sec:org74cb520}
These guidelines result from extensive discussions that took place over several
years. Special thanks to Alain Girault, Morane Gruenpeter, Julia Lawall, Arnaud
Legrand and Nicolas Rougier for their precious feedback on earlier versions of this document.

\clearpage
\printbibliography

\appendix\clearpage


\section{Appendix: Reference for SWHID identifiers}
\label{sec:org24c309c}
\label{sec:identifiers}

The SWHID identifier schema is \href{https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html}{fully documented online} and is discussed in the
article \cite{swhipres2018}, but we reproduce here for completeness an excerpt of the
documentation.\\


\noindent A SWHID consists of two separate parts, a mandatory \emph{core identifier} that can
 point to any software artifact (or “object”) available in the Software Heritage
 archive, and an \emph{optional list of qualifiers} that allows to specify the context
 where the object is meant to be seen and point to a subpart of the object
 itself.
\subsection{Syntax}
\label{sec:org681c060}
 Syntactically, SWHIDs are generated by the \verb|<identifier>|
 entry point of the EBNF grammar given in Table~\ref{tab:grammar}.

 \begin{table*}[h!]
 \caption{EBNF grammar of Software Heritage persistent identifiers}
 \label{tab:grammar}
 \small
 \begin{grammar}
<identifier> ::= <identifier_core> [ <qualifiers> ] 

<identifier_core> ::= `swh' `:' <scheme_version> `:' <object_type> `:' <object_id> 

<scheme_version> ::= `1' 

<object_type> ::= `snp'  |  `rel' | `rev' | `dir' | `cnt'

<object_id> ::= 40 * <hex_digit>   (* intrinsic object id, as hex-encoded SHA1 *)

<dec_digit> ::= `0' | `1' | `2' | `3' | `4' | `5' | `6' | `7' | `8' | `9' 

<hex_digit> ::= <dec_digit> | `a' | `b' | `c' | `d' | `e' | `f' 

<qualifiers> := `;' <qualifier> [ <qualifiers> ] 

<qualifier> ::=
    <context_qualifier> | <fragment_qualifier>

<context_qualifier> ::= <origin_ctxt> | <visit_ctxt> | <anchor_ctxt> | <path_ctxt>

<origin_ctxt> ::= `origin' `=' <url_escaped> 

<visit_ctxt> ::= `visit' `=' <identifier_core> 

<anchor_ctxt> ::= `anchor' `=' <identifier_core> 

<path_ctxt> ::= `path' `=' <path_absolute_escaped> 

<fragment_qualifier> ::= `lines' `=' <line_number> [`-' <line_number>] 

<line_number> ::= <dec_digit> + 

<url_escaped> ::= (* RFC 3987 IRI *)

<path_absolute_escaped> ::= (* RFC 3987 absolute path *)
 \end{grammar}
 \end{table*}

Where \verb|<path_absolute_escaped>| is an \verb|<ipath-absolute>| from RFC 3987, and
\verb|<url_escaped>| is a RFC 3987 IRI. 
In either case all occurrences of \verb|;| (and \verb|%|, as required by the RFC) 
have been percent-encoded (as \verb|%3B| and \verb|%25| respectively).
Other characters can be percent-encoded, e.g., to improve readability and/or embeddability of SWHID in other contexts.

\subsection{Semantics}
\label{sec:org8c7532d}
The \texttt{swh} prefix, which is IANA registered, makes explicit that these identifiers are related to
Software Heritage, and the colon (\verb|:|) is used as separator between the
logical parts of identifiers. The scheme version (currently \verb|1|) is the
current version of this identifier scheme.

A persistent identifier points to a single object, whose type is explicitly
captured by \verb|<object_type>|:
\begin{description}
\item[snp] identifiers points to snapshots,
\item[rel] to releases,
\item[rev] to revisions,
\item[dir] to directories,
\item[cnt] to contents.
\end{description}

The actual object pointed to is identified by the intrinsic identifier
\verb|<object_id>|, which is a hex-encoded (using lowercase ASCII characters)
SHA1~\cite{SHA1} computed on the content and metadata of the object
itself.\footnote{See
  \url{https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html}
  for more details.}

\subsection{Git compatibility}
\label{sec:org9ffd8ed}
Intrinsic object identifiers for contents, directories, revisions, and releases
are, at present, compatible with the Git way of computing identifiers for its
objects. A Software Heritage content identifier will be identical to a Git blob
identifier of any file with the same content, a Software Heritage revision
identifier will be identical to the corresponding Git commit identifier,
etc. This is not the case for snapshot identifiers as Git doesn’t have a
corresponding object type.\\

Git compatibility is incidental and is not guaranteed
to be maintained in future versions of this scheme (or Git), but is a convenient
feature for developers, for the time being.

\subsection{Examples}
\label{sec:orgc0086cf}
Here are a few interesting examples of what the Software Heritage
\emph{core identifiers} look like.
It is possible to access the corresponding artefact by prepending to the identifier the prefix of the
Software Heritage resolver: 
 \texttt{https://archive.softwareheritage.org/}

\vspace{.5em}

\begin{tcolorbox}
\href{https://archive.softwareheritage.org/swh:1:cnt:94a9ed024d3859793618152ea559a168bbcbb5e2}
{swh:1:cnt:94a9ed024d3859793618152ea559a168bbcbb5e2}
\end{tcolorbox}
points to the content of a file containing the full text of the GPL3 license\\
\begin{tcolorbox}
\href{https://archive.softwareheritage.org/swh:1:dir:d198bc9d7a6bcf6db04f476d29314f157507d505}
{swh:1:dir:d198bc9d7a6bcf6db04f476d29314f157507d505}
\end{tcolorbox}
points to a directory containing the source code of the Darktable photography
application as it was at some point on 4 May 2017\\
\begin{tcolorbox}
\href{https://archive.softwareheritage.org/swh:1:rev:309cf2674ee7a0749978cf8265ab91a60aea0f7d}
{swh:1:rev:309cf2674ee7a0749978cf8265ab91a60aea0f7d}
\end{tcolorbox}points to a commit in the development history of Darktable,
dated 16 January 2017, that added undo/redo supports for masks\\
\begin{tcolorbox}
\href{https://archive.softwareheritage.org/swh:1:rel:22ece559cc7cc2364edc5e5593d63ae8bd229f9f}
{swh:1:rel:22ece559cc7cc2364edc5e5593d63ae8bd229f9f}
\end{tcolorbox}
points to Darktable release 2.3.0, dated 24 December 2016\\
\begin{tcolorbox}
\href{https://archive.softwareheritage.org/swh:1:snp:c7c108084bc0bf3d81436bf980b46e98bd338453}
{swh:1:snp:c7c108084bc0bf3d81436bf980b46e98bd338453}
\end{tcolorbox}
points to a snapshot of the entire Darktable Git repository taken on 4 May 2017
from GitHub.


\subsection{Qualifiers}
\label{ssec:context}

The semi-colon (\verb|;|) is used as separator between the core identifier
and the optional qualifiers, as well as between qualifiers.
Each qualifier is specified as a key/value pair, using \verb|=| as a separator.

\noindent The following qualifiers are available:

\begin{description}
\item[origin]: the software origin where an object has been found or observed in the wild, as an URI;

\item[visit]: the core identifier of a snapshot corresponding to a specific visit of a repository containing the designated object;

\item[anchor]: a designated node in the Merkle DAG relative to which a path to the object is specified, as the core identifier of a directory, a revision, a release or a snapshot;

\item[path]: the absolute file path, from the root directory associated to the anchor node, to the object; when the anchor denotes a directory or a revision, and almost always when it’s a release, the root directory is uniquely determined; when the anchor denotes a snapshot, the root directory is the one pointed to by HEAD (possibly indirectly), and undefined if such a reference is missing;

\item[lines]: line number(s) of interest, usually within a content object
\end{description}

\subsection{Examples with qualifiers}
\label{sec:orgbdd7e18}
The following SWHID points to the source code root directory of the game Quake III
Arena\footnote{See \url{https://en.wikipedia.org/wiki/Quake_III_Arena}} with
the origin URL where it was found
\begin{tcolorbox}
\href{https://archive.softwareheritage.org/swh:1:dir:c6f07c2173a458d098de45d4c459a8f1916d900f;origin=https://github.com/id-Software/Quake-III-Arena/}
{swh:1:dir:c6f07c2173a458d098de45d4c459a8f1916d900f; \endgraf
 origin=https://github.com/id-Software/Quake-III-Arena}
\end{tcolorbox}

And the following SWHID points to a comment fragment in an example program for the OCamlP3l parallel programming library.\\
\begin{tcolorbox}
\href{https://archive.softwareheritage.org/swh:1:cnt:4d99d2d18326621ccdd70f5ea66c2e2ac236ad8b;origin=https://gitorious.org/ocamlp3l/ocamlp3l_cvs.git;visit=swh:1:snp:d7f1b9eb7ccb596c2622c4780febaa02549830f9;anchor=swh:1:rev:2db189928c94d62a3b4757b3eec68f0a4d4113f0;path=/Examples/SimpleFarm/simplefarm.ml;lines=9-15}
{swh:1:cnt:4d99d2d18326621ccdd70f5ea66c2e2ac236ad8b; \endgraf
  origin=https://gitorious.org/ocamlp3l/ocamlp3l\_cvs.git; \endgraf
  visit=swh:1:snp:d7f1b9eb7ccb596c2622c4780febaa02549830f9; \endgraf
  anchor=swh:1:rev:2db189928c94d62a3b4757b3eec68f0a4d4113f0; \endgraf
  path=/Examples/SimpleFarm/simplefarm.ml;lines=9-15}
\end{tcolorbox}
\end{document}
